import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

ds = pd.read_csv('original\Machine Learning A-Z (Codes and Datasets)\Part 2 - Regression\Section 4 - Simple Linear Regression\Python\Salary_Data.csv')

"""Get Feature and dependent variable"""

X = ds.iloc[:,:-1].values
Y = ds.iloc[:,-1].values

print (X)
print (Y)

from sklearn.model_selection import train_test_split
x_tr, x_tst, y_tr, y_tst = train_test_split(X, Y, test_size = 0.2)

"""Create linear regressor model"""

from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(x_tr,y_tr)

"""Predict output test data set"""

y_pred = regressor.predict(x_tst)
print (y_pred)

"""Plot graph: with subplot"""

fig = plt.figure()
plt.subplot(221)
plt.scatter(x_tr,y_tr, color = 'red')
plt.plot(x_tr,regressor.predict(x_tr), color = 'blue')
plt.xlabel ('Experience')
plt.ylabel('Salary')
plt.title("Experience vs Salary Linear regression")


plt.subplot(222)
plt.ylabel('Salary')
plt.xlabel('Experience')
plt.scatter(x_tst,y_tst,color = 'red')
plt.plot(x_tst,y_pred, color = 'blue')
plt.show()